Naistalujte si git/mozna ho uz mate. Nejlepe pro zacatek ovladat pres command line textovymi prikazy.<br>
Zaregistrujte se na gitlab.com , pote budete moct doufam, pozadat o pristup k tomu repository na cviceni<br>
<br>
git clone  https://gitlab.com/AlgoritmyII/summer2018.git <br>
cd summer2018 <br>
make/copy your new directory NEW_DIR - pouzijte svoje universitni cislo (PAP0084, NAM007)<br>
git add NEW_DIR/* <br>
git commit -m "komentar k tomu, co jste pridali" <br>
git push -u origin master <br>

<br>
Informace o zmenach:
git status<br>
"Hlavni cyklus": "add -> commit -> push"<br>

<br>
Zkontrolujte, jestli jste obsah spravne nahrali do git repository: https://gitlab.com/AlgoritmyII/summer2018 <br>
<br><br>
Vice informaci si pripadne dohledejte na internetu.
